from subprocess import check_call
from charms.reactive import when, when_not, when_any, is_flag_set, set_flag, clear_flag, endpoint_from_flag
from charms.templating.jinja2 import render
from charmhelpers.core.hookenv import resource_get, status_set, log
import os
import stat
import math
from psutil import virtual_memory
import shutil
from subprocess import run as sub_run
import socket

@when_not('druid-master.installed')
def install_druid_master():
    memory_check = check_sufficient_memory()
    if not memory_check[0]:
        log('Can not start MiddleManager, server has '
            + memory_check[1] + 'GB of RAM, but Master requires at least ' + memory_check[2] + 'GB')
        status_set('blocked', 'Master has insufficient RAM '
                              '(has ' + memory_check[1] + 'GB, requires ' + memory_check[2] + 'GB)')
    else:
        archive = resource_get("druid")
        if not os.path.isdir('/opt/druid_master'):
            os.mkdir('/opt/druid_master')
        cmd = ['tar', 'xfz', archive, '-C', '/opt/druid_master', '--strip', '1']
        check_call(cmd)

        archive = resource_get("mysql-jar")
        shutil.copy(archive, '/opt/druid_master/lib/')

        render('druid_master', '/etc/init.d/druid_master')
        render('druid_logrotate', '/etc/logrotate.d/druid_logrotate')
        st = os.stat('/etc/init.d/druid_master')
        os.chmod('/etc/init.d/druid_master', st.st_mode | stat.S_IEXEC)
        status_set('waiting', 'Waiting for config file')
        set_flag('druid-master.installed')

@when('druid-master.installed', 'endpoint.config.new_config')
def configure_druid_master():
    with open('/opt/druid_master/conf/druid/cluster/_common/common.runtime.properties', 'r') as c:
        old_conf = c.read()

    config = endpoint_from_flag('endpoint.config.new_config')
    new_conf = config.get_config()
    hostname = socket.gethostname()
    IPAddr = socket.gethostbyname(hostname)
    new_conf = new_conf.replace("druid.host=", "druid.host="+IPAddr+"\n")
    log('NEW CONF '+ new_conf)
    if old_conf != new_conf:
        log('New config detected! Resetting Druid Master.')
        if is_flag_set('druid-master.configured'):
            clear_flag('druid-master.configured')

        status_set('maintenance', 'Configuring Master')

        config_file = open('/opt/druid_master/conf/druid/cluster/_common/common.runtime.properties', 'w')
        config_file.write(new_conf)
        config_file.close()

        # write_runtime_properties()
        write_jvm_config()

        set_flag('druid-master.configured')
        set_flag('druid-master.new_config')
        status_set('maintenance', 'Druid Master configured, waiting to start process...')

@when('druid-master.installed', 'endpoint.config.new_hdfs_files')
@when_not('druid-master.hdfs_configured')
def configure_hdfs_files():
    hdfs = endpoint_from_flag('endpoint.config.new_hdfs_files')
    new_hdfs_files = hdfs.get_hadoop_files()

    status_set('maintenance', 'Copying HDFS XML Files.')

    with open('/opt/druid_master/conf/druid/cluster/_common/core-site.xml', 'w') as f:
        f.write(new_hdfs_files[0])
    with open('/opt/druid_master/conf/druid/cluster/_common/hdfs-site.xml', 'w') as f:
        f.write(new_hdfs_files[1])
    with open('/opt/druid_master/conf/druid/cluster/_common/mapred-site.xml', 'w') as f:
        f.write(new_hdfs_files[2])
    with open('/opt/druid_master/conf/druid/cluster/_common/yarn-site.xml', 'w') as f:
        f.write(new_hdfs_files[3])

    if is_flag_set('druid-master.configured'):
        clear_flag('druid-master.configured')

    set_flag('druid-master.hdfs_configured')
    set_flag('druid-master.new_config')
    status_set('waiting', 'HDFS files copied. Waiting...')


@when_any('druid-master.configured', 'druid-master.hdfs_configured')
@when('java.ready', 'druid-master.new_config')
def run_druid_master(java):
    restart_master()
    clear_flag('druid-master.new_config')


def start_master():
    status_set('maintenance', 'Starting Master...')
    code = sub_run(['/etc/init.d/druid_master', 'start'], cwd='/opt/druid_master')
    if code.returncode == 0:
        set_flag('druid-master.running')
        status_set('active', 'Master running')
    else:
        status_set('blocked', 'Error starting Master')


def stop_master():
    status_set('maintenance', 'Stopping Master')
    code = sub_run(['/etc/init.d/druid_master', 'stop'], cwd='/opt/druid_master')
    if code.returncode == 0:
        clear_flag('druid-master.running')
        status_set('waiting', 'Master stopped')
    else:
        status_set('blocked', 'Error stopping Master')

def restart_master():
    status_set('maintenance', 'Restarting Master...')
    stop_master()
    start_master()

def write_jvm_config():

    mem_gb = check_system_memory() // (1024 ** 3)  # Total memory of server, floor division for additional overhead
    Xms = math.ceil(check_minimum_memory() / (1024 ** 3))  # Minimum memory requirement for Druid in GB
    Xmx = max(Xms, math.floor(mem_gb - 1))  # Allocate maximum memory from total, subtract 1GB for overhead or set Xms

    context = {
        'Xms': Xms,
        'Xmx': Xmx
    }

    render('jvm.config', '/opt/druid_master/conf/druid/cluster/master/coordinator-overlord/jvm.config', context)


def check_sufficient_memory():
    """
    This method checks to make sure there is sufficient memory before allowing the charm to continue installing.
    """

    # Get minimum memory requirement for the heap. Round up since we'll round up when allocating Xms
    heap = math.ceil(check_minimum_memory() // (1024 ** 3))

    # Get total memory of the system
    mem = check_system_memory() // (1024 ** 3)

    # If the memory requirements exceed the amount available
    if heap > mem:
        return False, str(mem), str(int(heap))
    else:
        return True, 0, 0


def check_system_memory():
    """
    Returns the total system memory in bytes.
    """

    mem = virtual_memory()
    return mem.total


def check_minimum_memory():
    """
    Calculates the minimum memory requirement for this Druid Broker instance. Rule of thumb is:

            Minimum Requirement (bytes) = sizeBytes * (numThreads + numMergeBuffers + 1)

    However, we provide an extra GB of headroom on the low end to ensure that Druid Broker will be able to run.
    This additional GB is added within the return statement.

    :return: The minimum memory requirements (essentially Xms) of this Druid instance in bytes.
    """

    if os.cpu_count() > 1:
        numThreads = os.cpu_count() - 1
    else:
        numThreads = 1

    # numMergeBuffers is max of 2, numThreads/4
    if numThreads // 4 > 2:
        numMergeBuffers = numThreads // 4
    else:
        numMergeBuffers = 2

    sizeBytes = 1073741824  # 1 GB, parametize this later

    return sizeBytes * (numMergeBuffers + numThreads + 1) + 1073741824
